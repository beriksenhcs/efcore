﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCore
{
    [Table("Agency")]
    public class Agency : ModelBase
    {
        public int AgencyId { get; set; }
        public string Name { get; set; }
        public ICollection<AgencyAddress> AgencyAddresses { get; set; }
    }
}
