﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EFCore
{
    [Table("AgencyAddress")]
    public class AgencyAddress : ModelBase
    {
        public int AgencyAddressId { get; set; }
        public int AgencyId { get; set; }
        public string Address { get; set; }
    }
}
