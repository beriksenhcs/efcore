﻿namespace EFCore
{
    public interface IRepositoryContainer
    {
        IRepository<AgencyAddress> AgencyAddressRepository { get; }
        IRepository<Agency> AgencyRepository { get; }
    }
}