﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace EFCore
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(params object[] keyValues);

        Task DeleteAsync(T entity);

        Task DeleteAsync(params object[] keyValues);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);
    }
}
