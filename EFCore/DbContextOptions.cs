﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore
{
    public class DbContextOptions
    {
        public string ConnectionString { get; set; }
    }
}
