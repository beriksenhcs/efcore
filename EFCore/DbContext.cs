﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EFCore
{
    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbContext(DbContextOptions<DbContext> dbContextOptions)
            : base(dbContextOptions)
        { }

        public DbSet<Agency> Agencies { get; set; }
        public DbSet<AgencyAddress> AgencyAddresses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agency>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<AgencyAddress>().HasQueryFilter(p => !p.IsDeleted);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaving();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void OnBeforeSaving()
        {
            foreach (var entry in ChangeTracker.Entries<ModelBase>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        entry.CurrentValues["CreatedOn"] = DateTime.UtcNow;
                        entry.CurrentValues["CreatedBy"] = Guid.NewGuid().ToString(); //Refactor with some user context
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        goto default;
                    default:
                        entry.CurrentValues["ModifiedOn"] = DateTime.UtcNow;
                        entry.CurrentValues["ModifiedBy"] = Guid.NewGuid().ToString(); //Refactor with some user context
                        break;
                }
            }
        }
    }
}
