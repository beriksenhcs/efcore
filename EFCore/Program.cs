﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace EFCore
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json", true, true);
            var configuration = configurationBuilder.Build();

            var myLoggerFactory = new LoggerFactory(new[] { new ConsoleLoggerProvider((_, __) => true, true) });
            serviceCollection.AddSingleton<ILoggerFactory>(myLoggerFactory);
            serviceCollection.AddRepositories(configuration.GetSection("SQL"));

            var services = serviceCollection.BuildServiceProvider();

            var agencyId = await CreateAgencyAsync(services);
            await UpdateAgencyAsync(services, agencyId);
            var agencies = await GetAllAgenciesAsync(services);
            await DeleteAgencyAsync(services, agencyId);
            agencies = await GetAllAgenciesAsync(services);
        }

        private static async Task<IEnumerable<Agency>> GetAllAgenciesAsync(ServiceProvider services)
        {
            using (var scope = services.CreateScope())
            {
                var repoContainer = scope.ServiceProvider.GetRequiredService<IRepositoryContainer>();
                return await repoContainer.AgencyRepository.GetAllAsync();
            }
        }

        private static async Task DeleteAgencyAsync(ServiceProvider services, int agencyId)
        {
            using (var scope = services.CreateScope())
            {
                var repoContainer = scope.ServiceProvider.GetRequiredService<IRepositoryContainer>();
                await repoContainer.AgencyRepository.DeleteAsync(agencyId);
            }
        }

        static async Task<int> CreateAgencyAsync(IServiceProvider services)
        {
            using (var scope = services.CreateScope())
            {
                var repoContainer = scope.ServiceProvider.GetRequiredService<IRepositoryContainer>();

                var newAgency = new Agency
                {
                    Name = "My New Agency"
                };
                await repoContainer.AgencyRepository.AddAsync(newAgency);
                return newAgency.AgencyId;
            }
        }

        static async Task UpdateAgencyAsync(IServiceProvider services, int agencyId)
        {
            using (var scope = services.CreateScope())
            {
                var repoContainer = scope.ServiceProvider.GetRequiredService<IRepositoryContainer>();

                var agency = await repoContainer.AgencyRepository.GetByIdAsync(agencyId);
                agency.Name = "A different name";
                await repoContainer.AgencyRepository.UpdateAsync(agency);
            }
        }
    }
}
