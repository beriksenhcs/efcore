﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace EFCore
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException(nameof(config));
            }
        
            var dbContextOptions = new DbContextOptions();
            config.Bind(dbContextOptions);

            if (string.IsNullOrWhiteSpace(dbContextOptions.ConnectionString))
                throw new ArgumentNullException(nameof(dbContextOptions.ConnectionString));

            services.AddDbContext<DbContext>((options) =>
            {
                options.UseSqlServer(dbContextOptions.ConnectionString);
            });
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IRepositoryContainer, RepositoryContainer>();

            return services;
        }
    }
}
