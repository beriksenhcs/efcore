﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace EFCore
{
    public class RepositoryContainer : IRepositoryContainer
    {
        private readonly IServiceProvider _serviceProvider;

        public RepositoryContainer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
        }

        public IRepository<Agency> AgencyRepository => _serviceProvider.GetRequiredService<IRepository<Agency>>();
        public IRepository<AgencyAddress> AgencyAddressRepository => _serviceProvider.GetRequiredService<IRepository<AgencyAddress>>();
    }
}
