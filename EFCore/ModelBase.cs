﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore
{
    public abstract class ModelBase
    {
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDeleted { get; set; }
    }
}
